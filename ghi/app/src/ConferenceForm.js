
import React, { useState, useEffect } from 'react';

function ConferenceForm(props){
    const [ name, setName ] = useState('');
    const [ starts, setStart ] = useState('');
    const [ ends, setEnds ] = useState('');
    const [ max_presentations, setMaxPresentations ] = useState('');
    const [ description, setDescription ] = useState('');
    const [ max_attendees, setMaxAttendees ] = useState('');
    const [ location, setLocation ] = useState();
    const [ locations, locFetch ] = useState([]);

    const handleSubmit = async (sumbit) => {
        sumbit.preventDefault()
        //note* if you want to have consistent naming convetion we would use camelCasing
        // and pass that into our body "{max_attendees: maxAttendee}"
        const data  = {name, starts, ends, max_presentations, description, max_attendees, location}
        console.log(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            const newConference = await response.json();
            console.log(newConference)
        }
    }

    useEffect(() => {
        const fetchLocations = async () => {
            //get the locations data to map to later
            const url = 'http://localhost:8000/api/locations/'
            const response = await fetch(url)
            const data = await response.json()
            // console.log(data.locations)
            locFetch(data.locations)
        }

        fetchLocations()
    }, [])



    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Name" value={name} onChange={(event) => setName(event.target.value)} required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Starts" value={starts} onChange={(event) => setStart(event.target.value)} required type="date" name="starts" id="start" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Ends" value={ends} onChange={(event) => setEnds(event.target.value)} required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" value={description} className="form-label">Description</label>
                <textarea className="form-control" onChange={(event) => setDescription(event.target.value)} id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="max_presentations" value={max_presentations} onChange={(event) => setMaxPresentations(event.target.value)} required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="max_attendees" value={max_attendees} onChange={(event) => setMaxAttendees(event.target.value)} required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select required id="location" value={location} onChange={(event) => setLocation(event.target.value)} name="location" className="form-select">
                  {locations.map(loc => (
                  <option key={loc.id} value={loc.id}>{loc.name}</option>))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>

    )
}

export default(ConferenceForm)
