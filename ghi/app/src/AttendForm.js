import React, { useEffect, useState } from 'react';

function AttendForm() {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [conference, setConference] = useState('')
    const [conferences, setConferences] = useState([])

    const handleSubmit = async (e) => {
        e.preventDefault()
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({
                name: name,
                email: email,
                conference: conference
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);
            const formTag = document.getElementById('create-attendee-form');
            const successTag = document.getElementById('success-message')
            formTag.classList.add("d-none")
            successTag.classList.remove("d-none")

        }
    }

    useEffect(() => {
        const fetchConferences = async () => {
            const url = 'http://localhost:8000/api/conferences/'
            const response = await fetch(url)
            if (response.ok) {
                await new Promise(r => setTimeout(r, 2000))
                const data = await response.json()
                setConferences(data.conferences)
                const selectTag = document.getElementById('conference');
                const loadingTag = document.getElementById('loading-conference-spinner')
                loadingTag.classList.add("d-none")
                selectTag.classList.remove("d-none")
            }
        }

        fetchConferences()

    }, [])

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="./logo.svg" />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-attendee-form">
                                <h1 className="card-title">It's Conference Time!</h1>
                                <p className="mb-3">
                                    Please choose which conference
                                    you'd like to attend.
                                </p>
                                <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select value={conference} onChange={(event) => setConference(event.target.value)} name="conference" id="conference" className="form-select d-none" required>
                                        <option value="">Choose a conference</option>
                                        {conferences.map(conference => {
                                            return <option key={conference.href} value={conference.href}>{conference.name}</option>
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Now, tell us about yourself.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input value={name} onChange={(event) => setName(event.target.value)} required placeholder="Your full name" type="text" id="name"
                                                name="name" className="form-control" />
                                            <label htmlFor="name">Your full name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input value={email} onChange={(event) => setEmail(event.target.value)} required placeholder="Your email address" type="email" id="email"
                                                name="email" className="form-control" />
                                            <label htmlFor="email">Your email address</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">I'm going!</button>
                            </form>
                            <div className="alert alert-success d-none" id="success-message">
                                Congratulations! You're all signed up!
                                <a type="button" className="btn btn-primary btn-lg btn-block"
                                    href="attend-conference.html">Attend another conference</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default AttendForm
