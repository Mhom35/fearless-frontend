window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
      const selectSpinner = document.getElementById('loading-conference-spinner');
      selectSpinner.classList.add('d-none');
    //   .spinner-grow text-secondary
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none')
    }
    const formTag = document.getElementById('create-attendee-form');
    const alertTag = document.getElementById('success-message');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)

        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(attendeesUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();// reset our form to original state(not like the territory)
            const newAttendees = await response.json();//we save our post info
            }
            alertTag.classList.remove('d-none')
            formTag.classList.add('d-none')
        });



  });
