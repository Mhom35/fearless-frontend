window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const dropDownElement = document.getElementById('state');
        for(let state of data.states){
            let option = document.createElement("option")
            option.value = state.abbreviation;
            option.innerHTML = state.state_name;

            dropDownElement.appendChild(option);
        }}

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();// reset our form to original state(not like the territory)
            const newLocation = await response.json();//we save our post info
            }
        });
});
