function createCard(name, description, pictureUrl, starts, end, subtitle) {
    return `
      <div class="card">
      <div class="shadow p-3 mb-5 bg-body rounded">Regular shadow
            <img src=${pictureUrl} class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
                <p class="card-text">${description}</p>

            </div>
            <div class="card-footer text-muted">${starts}-${end}
            </div>
        </div>
      </div>
    `;
  }

function alertComponent(){
    return `
    <div class="alert alert-danger d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
            An example danger alert with an icon
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok');
        alert("Error")
      } else {
        const data = await response.json();
        let index = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            let starts = new Date(details.conference.starts).toLocaleDateString()
            let ends = new Date(details.conference.ends).toLocaleDateString()
            // console.log()
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            console.log(details)
            // console.log(html)
            const column = document.querySelector(`#col-${index % 3}`);
            column.innerHTML += html;
            index+=1
          }
        }

      }
    } catch (e) {
        console.error("got an error", e)
        const alertHTML = alertComponent()
        const alertMessage = document.querySelector('.alert-message')
        alertMessage.innerHTML = alertHTML
    }

  });

//   http://localhost:8000/api/conferences/


// const conference = data.conferences[0];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;

//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok){
//                 const details = await detailResponse.json();
//                 const descTag = document.querySelector('.card-text');
//                 descTag.innerHTML = details.conference.description
//                 console.log(details)

//                 const imageTag = document.querySelector('.card-img-top');
//                 imageTag.src = details.conference.location.picture_url;
